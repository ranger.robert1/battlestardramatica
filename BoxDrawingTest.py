class BC:
    #Box Characters
    BOX_S = chr(9474)
    BOX_T = chr(9472)
    BOX_TR = chr(9488)
    BOX_TL = chr(9484)
    BOX_BR = chr(9496)
    BOX_BL = chr(9492)
    #Box Joiners, Left, Right, and Cross. Then top/bottom
    BOX_JSL = chr(9500)
    BOX_JSR = chr(9508)
    BOX_JSC = chr(9532)
    BOX_JST = chr(9516)
    BOX_JSB = chr(9524)

class BBC:
    BOX_S = chr(9553)
    BOX_T = chr(9552)
    BOX_TR = chr(9559)
    BOX_TL = chr(9556)
    BOX_BR = chr(9565)
    BOX_BL = chr(9562)
    #Box Joiners, Left, Right, and Cross. Then top/bottom
    BOX_JSL = chr(9571)
    BOX_JSR = chr(9568)
    BOX_JSC = chr(9580)
    BOX_JST = chr(9574)
    BOX_JSB = chr(9577)

class JC:
    JN = 0 #NO JOIN
    J  = 1
    JC = 2 #CENTER
    JT = 3 #TOP
    JB = 4 #BOTTOM
    JE = 5 #EMPTY


def genSquareGrid(lst):
    #TODO:
    #just handling bottom decorations. NO SHORTCUTS!
    grid = ""
    rows = len(lst)

    #keep track of previous, and next column count
    prev_columns = -1 #first row is implied to have no previous rows.
    next_columns = 0
    extras = []
    
    for i in range(0,rows): # save the first and last for seperate processing
        columns = len(lst[i])
        if (i+1 < rows):
            next_columns = len(lst[i+1])
        else:
            next_columns = -1
        #jump right in, the first row isn't so special.
        c_rows = []
        for j in range(0,columns):
            decoration = -1
            #decorate left 
            elem = genLBorder(lst[i][j])
            hasLeft = (j > 0)
            hasUpperLeft = ( j <= prev_columns)
            hasUpper = (j < prev_columns)
            ##decorate left upper##
            #check left -> upper left.
            if (hasLeft): #check if there's anything to the left...
                if (hasUpperLeft): #if there's anything to the upper left. implies something is above as well
                    ## if both, JC
                    decoration = JC.JC
                ## if just left, joint top
                else:
                    decoration = JC.JT
                #left, and upper but not upper left shouldn't happen so it should fall through next if applicable.
            if (hasUpper and not hasLeft): #check if there's anything immediatly above, but nothing left
                decoration = JC.J
                ## if neither, check upper and apply J if so.

            if (not(hasUpper or hasLeft or hasUpperLeft)):
                decoration = JC.JN

            #generate lower decoration... if applicable.
            b_decoration_l = -1
            b_decoration_r = -1
            hasRight = (j < columns-1)
            hasBottom = (j < next_columns)
            hasBottomLeft = ( j<=next_columns)
            #print(str(j) + " " + str(next_columns))
            #and not (next_columns == -1))
            elem = genTBorder(elem,decoration,JC.JE)
            if(not hasBottom):
                if(hasRight):
                    b_decoration_r = JC.JB
                else:
                    b_decoration_r = JC.JN

                if (hasBottomLeft):
                    b_decoration_l = JC.JE
                elif(hasLeft):
                    b_decoration_l = JC.JE
                else:
                    b_decoration_l = JC.JN
                elem = genBBorder(elem,b_decoration_l,b_decoration_r,-1)
                    

            #decorate upper
            #print(str(decoration) + str(hasLeft) + str(hasUpper) + str(hasUpperLeft))
            #print(str(b_decoration_l) + " " + str(b_decoration_r) + str(hasBottom) + str(hasRight) + str(hasLeft))
            #add decorated element to row
            c_rows += [elem]

        decoration_r = 0
        #add right/right upper decoration after row is completed
        hasUp = (j <= prev_columns-1)
        hasUpRight = (j < prev_columns-1)
        #hasRight = (j < columns-1) #check if any elements to right (at end?) 
        ##find the decoration
        if(hasUp):
            if(hasUpRight or hasRight):
                decoration_r = JC.JC
            else:
                decoration_r = JC.J
        if (hasRight and not hasUpRight):
            decoration_r = JC.JT
        elif (not (hasRight or hasUp or hasUpRight)):
        #else:
            decoration_r = JC.JN
        #print(str(decoration_r) + str(hasUp) + str(hasUpRight) + str(hasRight) + str(j) + " " + str(prev_columns))
        temp = genTBorder(genRBorder(genLBorder(lst[i][j])),decoration,decoration_r)
        if (not hasBottom):
            temp = genBBorder(temp,b_decoration_l,b_decoration_r)
        c_rows[-1] = temp
        #add completed row with newline to the grid.
        #Now, merge the lines before continuing.
        merged,extras = joinGrid(c_rows,extras)
#        print(merged)
        grid += merged
        #grid += row + "\n"
        #update previous columns to current
        prev_columns = columns
    print(grid)

def joinGrid(lst,firstLineAdd):
#    length = len(lst[0].splitlines())
    lengths = []
    for i in range(len(lst)):
        lines = lst[i].splitlines()
        lengths.insert(0,len(lines))
    length = max(lengths)
    min_length = min(lengths)
    #print(length)
    #print(min_length)
    inequal_lengths = length == min_length
#    length = len(lst[0].splitlines())
    res = [""]*(length - (length-min_length))
    extras = []
    for i in range(len(lst)):
        lines = lst[i].splitlines()
        #print(lines)
        for j in range(len(lines)):
            #print(lines[j])
            if (j < min_length):
                res[j] += lines[j]
            else:
                extras.append(lines[j])
    #contact previous extras to current first line.
    for i in firstLineAdd:
        res[0] += i
    return ("\n".join(res) + "\n",extras)

def genLBorder(in_str):
    #split into lines
    lines = in_str.split("\n")
    for i in range(len(lines)):
        lines[i] = BC.BOX_S + lines[i] 
    return "\n".join(lines)

def genRBorder(in_str):
    #split into lines
    lines = in_str.split("\n")
    for i in range(len(lines)):
        lines[i] = lines[i] + BC.BOX_S
    return "\n".join(lines)

def genTDecoration(tl,tr):
    r,l = ("","")
    #RIGHT
    if (tr == JC.JN):
        r = BC.BOX_TR
    elif (tr == JC.J):
        r = BC.BOX_JSR
    elif (tr == JC.JC):
        r = BC.BOX_JSC
    elif (tr == JC.JT):
        r = BC.BOX_JST 
    elif (tr == JC.JE):
        r = BC.BOX_T
    #left
    if (tl == JC.JN):
        l = BC.BOX_TL
    elif (tl == JC.J):
        l = BC.BOX_JSL
    elif (tl == JC.JC):
        l = BC.BOX_JSC
    elif (tl == JC.JT):
        l = BC.BOX_JST 
    elif (tl == JC.JE):
        l = BC.BOX_T
    return (l,r)

def genBDecoration(bl,br):
    r,l = ("","")
    #RIGHT
    if (br == JC.JN):
        r = BC.BOX_BR
    elif (br == JC.J):
        r = BC.BOX_JSR
    elif (br == JC.JC):
        r = BC.BOX_JSC
    elif (br == JC.JB):
        r = BC.BOX_JSB 
    elif (br == JC.JE):
        #r = BC.BOX_T
        r=""
    #left
    if (bl == JC.JN):
        l = BC.BOX_BL
    elif (bl == JC.J):
        l = BC.BOX_JSL
    elif (bl == JC.JC):
        l = BC.BOX_JSC
    elif (bl == JC.JB):
        l = BC.BOX_JSB 
    elif (bl == JC.JE):
        #l = BC.BOX_T
        l=""
    return (l,r)

def genTBorder(in_str, tl=JC.JN, tr=JC.JN, str_joiner="\n"):
    #split into lines, and add a top line
    #how to customize the top/bottom decorations the easiest?
    #OPTIONS JOIN LEFT, JOIN RIGHT, OR JOIN BOTH
    #JOIN TOP JOIN BOTTOM
    lines = in_str.split("\n") #commented out to omptimize, remove after testing
    t_length = len(lines[0])-2 
    (l_dec, r_dec) = genTDecoration(tl,tr)
    lines.insert(0,l_dec + t_length*BC.BOX_T + r_dec)
    return str_joiner.join(lines)

def genBBorder(in_str, bl=JC.JN, br=JC.JN, offset=0,str_joiner="\n"):
    #assumes input shape is square?
    #split into lines, and add a bottom line
    lines = in_str.split("\n") #commented out to omptimize, remove after testing
    b_length = len(lines[-1])-2-offset
    (l_dec, r_dec) = genBDecoration(bl,br)
    lines.append(l_dec + b_length*BC.BOX_T + r_dec)
    return str_joiner.join(lines)

def genBorder(in_str):
    #assumes input shape is square?
    #split into lines, and add a top and bottom line
    lines = in_str.split("\n")
    t_length = len(lines[0]) 
    b_length = len(lines[-1]) 
    for i in range(len(lines)):
        lines[i] = BC.BOX_S + lines[i] + BC.BOX_S
        
    lines.insert(0,BC.BOX_TL + t_length*BC.BOX_T + BC.BOX_TR)
    lines.append(BC.BOX_BL + b_length*BC.BOX_T + BC.BOX_BR)
    return "\n".join(lines)

def genBoldBorder(in_str):
    #assumes input shape is square?
    #split into lines, and add a top and bottom line
    lines = in_str.split("\n")
    t_length = len(lines[0]) 
    b_length = len(lines[-1:]) 
    for i in range(len(lines)):
        lines[i] = BBC.BOX_S + lines[i] + BBC.BOX_S
        
    lines.insert(0,BBC.BOX_TL + t_length*BBC.BOX_T + BBC.BOX_TR)
    lines.append(BBC.BOX_BL + t_length*BBC.BOX_T + BBC.BOX_BR)
    return "\n".join(lines)


def genBoldBox(width, length):
    #Both width, and length must be at least 2. (No sides/bottoms for 2)
    c_w = width - 2
    c_l = length - 2
    box = ""
    #TOP
    box = BBC.BOX_TL + c_w*BBC.BOX_T + BBC.BOX_TR + "\n"
    box += c_l*(BBC.BOX_S + c_w*" " + BBC.BOX_S + "\n")
    box += BBC.BOX_BL + c_w*BBC.BOX_T + BBC.BOX_BR  
    #MIDDLE
    #BOTTOM
    return box

def genBox(width, length):
    #Both width, and length must be at least 2. (No sides/bottoms for 2)
    c_w = width - 2
    c_l = length - 2
    box = ""
    #TOP
    box = BC.BOX_TL + c_w*BC.BOX_T + BC.BOX_TR + "\n"
    box += c_l*(BC.BOX_S + c_w*" " + BC.BOX_S + "\n")
    box += BC.BOX_BL + c_w*BC.BOX_T + BC.BOX_BR  
    #MIDDLE
    #BOTTOM
    return box

test_list = [["AAAA\n"*3+"aaaa","BBBB\n"*3+"bbbb","CCCC\n"*3+"cccc"],
["DDDD\n"*3+"dddd","EEEE\n"*3+"eeee","FFFF\n"*3+"ffff"],
["GGGG\n"*3+"gggg","HHHH\n"*3+"hhhh","IIII\n"*3+"iiii"]]
genSquareGrid(test_list)

test_one = [["AAAA\n"*3+"aaaa"]]
genSquareGrid(test_one)

test_list2 = [
["AAAA\n"*3+"aaaa",],
["DDDD\n"*3+"dddd","EEEE\n"*3+"eeee"],
["GGGG\n"*3+"gggg","HHHH\n"*3+"hhhh","IIII\n"*3+"iiii","JJJJ\n"*3+"jjjj"],
["DDDD\n"*3+"dddd","EEEE\n"*3+"eeee"],
["AAAA\n"*3+"aaaa",]
]
genSquareGrid(test_list2)

test_cross = [["AAAA\n"*3+"aaaa","BBBB\n"*3+"bbbb"]]
genSquareGrid(test_cross)

test_light_lean = [["AAAA\n"*3+"aaaa","CCCC\n"*3+"cccc"]
,["BBBB\n"*3+"bbbb"]]
genSquareGrid(test_light_lean)

test_stack_end = [["AAAA\n"*3+"aaaa"],["BBBB\n"*3+"bbbb"]]
genSquareGrid(test_stack_end)

#a = "AAAA\nBBBB\nCCCC"
#b = "DDDD\nBEEE\nFFFF"
#a_x = (genBBorder(genTBorder(genLBorder(a),0,5),0,5))
#b_x = (genBBorder(genTBorder(genLBorder(b),3,5),4,5))
#r=""
#i = a_x.split("\n")
#j = b_x.split("\n")

#for cnt in range(len(j)):
#    r += i[cnt] + j[cnt] + "\n"

#print(r)
    

