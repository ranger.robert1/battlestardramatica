#GameDisplay.py
#Felt the need to move some display functions out of Main.py, getting cramped in there.
#First class to be implemented should be text display, allowing basic pieces of game state to be displayed as text.

from GameState import GameConstants
from GameState import GameVariables
from GameState import Game
from System import Planet
from Player import PC
import Fleets


class TextDisplay:

    def __init__(self,game):
        self.G = game
        self.grid = game.system.grid
    
    def PlayerNumPrompt(self):
        playerNum=0
        while (True):
            try:
                playerNum=int(input("Please enter number of players 1 - " + str(GameConstants.MAX_PLAYERS) + ": "))
                if(playerNum > GameConstants.MAX_PLAYERS or playerNum < 0 ):
                    print("invalid input!")                
                    continue
                break
            except:
                print("invalid input!")
        return playerNum

    def PlayerNamePrompt(self,playerNum):
        names = []
        for i in range(playerNum):
            names.append(str(input("Please enter player " + str(i+1) + ":")))
        return names

    def DisplayState(self):   
        self.DisplayGrid(self.grid)

    def DisplayGrid(self,grid):
        top = len(grid[0])*10*"_"+"_"
        print(top)
        for i in grid:
                print("_",end='')
                for j in i:
                    print(self.GenerateTile(j),end='')
                print()
        print(top)    

    ###
    #GenerateTile
    ##
    #Takes a planet as an argument, and generates a string represenation of the planet.
    ###
    def GenerateTile(self,planet):
        #Its either: Empty, a planet, or off map... for now.
        if (planet is None): #empty
            return "|xH: 000|_"
        elif (type(planet) is str):
             return "|-BORDR-|_"
        elif (planet.owner is None):
            return "|A" +  "H: " + str(planet.health) + "|_"
        else:
            return "|" + str(planet.owner.playerID) + "H: " + str(planet.health) + "|_"

    def UserInputMenu(self):
        p = GameVariables.currentPlayer
        print("It is now " + p.name + "'s turn!")
        print("1. Diplomacy")
        print("2. Fleet Management")
        print("3. Planet Management")
        while (True):
            try:
                pin = int(input("Please choose a selection: "))
                if (pin not in [1,2,3]):
                    print("Please try again.")
                    continue 
                break
            except:
                print("Please try again.")
        print(pin)
        if (pin == 1):
            print("Diplomacy not yet implemented!")
        elif(pin==2) :
            pfleets = p.fleets
            numfleets = pfleets.cnt
            print("Fleet management:")
            print("1. Attack")        
            print("2. Defend")
            print("3. Explore")
            print("4. Recall")
            print("5. Repair")
            print("6. List")
            while (True):
                try:
                    pin = int(input("Please choose a selection: "))
                    if (pin not in [1,2,3,4,5,6]):
                        print("Please try again.")
                        continue 
                    break
                except:
                    print("Please try again.")
            if(pin == 1): #ATTACK
                print("There are currently " + str(numfleets) + " fleet(s) to attack with.")
                counter = 1
                for i in p.fleets.fleet:
                    print(str(counter) + "." +str(i))
                    while (True):
                        try:
                            pin = int(input("Please choose a selection: "))
                            if (pin not in range(1,numfleets+1)):
                                print("Please try again.")
                                continue 
                            break
                        except:
                            print("please try again")
                print("Attacking with " + str(pfleets.fleet[pin-1].name) + ".")
                self.DisplayGrid(self.G.system.adjacentPlanets(pfleets.fleet[pin-1].location))
            elif(pin == 2): #DEFEND
                print("Pick a planet to defend...")

            elif(pin == 3): #EXPLORE
                print("Pick a planet to explore...")

            elif(pin == 4): #RECALL
                print("Recalling fleet to capital.")

            elif(pin == 5): #REPAIR
                print("Attempting repair...")
                #Display result after!

            elif(pin == 6): #LIST
                print("TODO")
                #Display fleet info
     

            else: 
                print("Not implemented yet")
            print(pin)
            
        else:
            print("Planet management not yet implemented!")
