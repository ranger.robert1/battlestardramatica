#GameState.py
#Manages all game interactions, moves, etc
class GameVariables:
    turn = 0 #turn counter
    pturn = 0  #should match playerid
    win = False
    currentPlayer = None
    players=[]
    nameGen = None #Initialized on start
    rand = None # Initialized on start

class GameConstants:
    MAX_PLAYERS=2
    #MAXIMUM SYSTEM DIMENSIONS - INITIALIZED ON SYSTEM GENERATION
    MAX_X = -1 
    MAX_Y = -1


from Player import PC
from System import Sys
from System import Planet
import random
from random import SystemRandom
from NameGenerator import NameGen


class Game:
    playerNum = 0
    #ai = [] #May be used later to seperate AI and PC

    def __init__(self,playerNum,names):
        #Setup Players
        self.playerNum = playerNum
        c = 1
        for i in names:
            GameVariables.players.insert(0,PC(i,c))
            c = c+1
        #setup RNG and name generator
        GameVariables.rand = random.SystemRandom()
        GameVariables.nameGen = NameGen(GameVariables.rand)

        #Generate world, and start fleets
        self.system = Sys()
        self.system.generateWorld()
        for i in GameVariables.players:
            i.createStarterShip()
        #Set player turn to 0 (no one's turn)
        GameVariables.pturn = 0
            

    #Steps the game state forward to the next player, given the current state.
    def next(self):
        if (GameVariables.pturn == self.playerNum):
            GameVariables.pturn=1
        else:
            GameVariables.pturn += 1
    
        GameVariables.currentPlayer = GameVariables.players[GameVariables.pturn-1]      
        #return checkWinner()
        #Mobalizing fleets
        

        #Diplomacy Loop (TODO)

        #Planet Management (TODO)
        
    def checkWinner():
        if(GameVariables.win):
            return currentPlayer
        else:
            return None

        #Empties the current player's turn queue and processes it's effect on the game state.
    def processTurn():
        print("TODO")
        #TODO

