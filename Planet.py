#class that implements planet functionality and attributes

import random

class Planet:
    owner = None
    troops = 0  #further defenses to come.
    resources = 0 #Only implementing gold in v1
    settled = False # only implement settlement/base infrastructure

    def __init__(self, owner):
        self.owner = owner
        self.resources = random.randint(50,100)
        if (owner is None):
            self.settled = False
            self.troops = 0
        else 
            self.settled = True
            self.troops = 500 #Set a constant in gamestate later?
