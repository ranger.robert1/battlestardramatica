#Player.py
#Class that manages players, extended to include AI later

from Fleets import Flt
from Fleets import FltGrp
from Fleets import FltTypes
from System import Planet

class PC:
    capital = None 
    planets = []
    def __init__(self, name, playerID):
        self.money = 0
        self.income = 0
        self.name = name
        self.playerID = playerID
        self.fleets = FltGrp()
   

    def setCapital(self,planet):
        self.capital = planet
        self.planets.insert(0,planet)
        planet.owner=self

    def createStarterShip(self):
        self.fleets.addFleet(Flt(FltTypes.FLT_EXPL,self.capital))
        
    
