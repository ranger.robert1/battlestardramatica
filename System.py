#Manages the world, and the state of planets with a system

from GameState import GameConstants
from GameState import GameVariables
#from GameState import Game
from Fleets import Flt
import random

class Sys:
    grid = []
    size=0
    shape=0 #TO IMPLEMENT

    def __init__(self):
        self.size = 5 #Default test size, two players one planet to fight over
        self.grid = [[None for i in range(self.size)] for j in range(self.size)] 
        GameConstants.MAX_X = self.size - 1
        GameConstants.MAX_Y = self.size - 1
        #print("TODO")
        #TODO

  #  def __init__(self, size, shape):
  #      self.size = size
  #      self.shape = shape
        #print("TODO")

    def generateWorld(self):
        #Generate players planets, and then size extra planets.
        for i in range(0,self.size):
            while (True):
                x = random.randint(0,self.size-1)
                y = random.randint(0,self.size-1)
                if (self.grid[x][y] is None) :
                    break
            p = Planet(None,x,y)
            self.grid[x][y] = p


        for i in GameVariables.players:
            #pick an empty planet location
            while (True):
                x = random.randint(0,self.size-1)
                y = random.randint(0,self.size-1)
                if (self.grid[x][y] is None) :
                    break
            p = Planet(i,x,y)
            self.grid[x][y] = p
            i.setCapital(p)
#        grid[1] = Planet()
        #print("TODO")
       #TODO 

 
    ###
    #adjacentPlanets
    ##
    # Takes it's self, and a location. Uses the system's grid
    # to pin point adjacent planets.
    # Returns a 3x3 list of adjacent planets (if applicable)
    ###
    def adjacentPlanets(self,location):
        start_x = location.x
        start_y = location.y
        ######
        #  0 1 2 x
        #0 a b c
        #1 d O e
        #2 f g h 
        #y

        a = (start_x-1 , start_y-1)
        b = (start_x, start_y-1)
        c = (start_x+1 , start_y-1)

        d = (start_x-1 , start_y)
        o = (start_x, start_y)
        e = (start_x+1 , start_y)

        f = (start_x-1 , start_y+1)
        g = (start_x, start_y+1)
        h = (start_x+1 , start_y+1)
        
        offsets = [[a,b,c],[d,o,e],[f,g,h]]
        adjacent_planets = [[None for i in range(3)] for j in range(3)] 
        #implement a proper planet type for emptyspace off map?
        for i in range(3):
            for j in range(3):
                #Check if calculated offset is off map...
                offset = offsets[j][i]
                if (self.inboundsCheck(offset[0],offset[1])):
                    adjacent_planets[i][j] = self.grid[offset[0]][offset[1]]
                else:
                    adjacent_planets[i][j] = "Offmap"
        return adjacent_planets 

   #Doesn't really need self, does it?
    def inboundsCheck(self,x,y):
        #If the x or y are above max, they're out of bounds.
        #If they're less than 0, they're also out of bounds.
        if (x > GameConstants.MAX_X or y > GameConstants.MAX_Y or x < 0 or y < 0):
            return False
        else:
            return True


class Planet:
    name = "Test" #Generate later?
    #owner = None
    #Coordinates coorespond to 
    x=-1
    y=-1
    health =0
    troops = 0  #further defenses to come.
    fltDefend = 0 #defense added by fleets
    defendingFleets=[]
    resources = 0 #Only implementing gold in v1
    settled = False # only implement settlement/base infrastructure

    def __init__(self, owner,x,y):
        self.name = GameVariables.nameGen.GeneratePlanetName()
        self.x = x
        self.y = y
        self.health = 100
        self.owner = owner
        self.resources = random.randint(50,100)
        self.fltDefend = 0
        self.defendingFleets = []
        if (owner is not None): #Might not even be necessary, depending on how you structure it
            self.settled = True
            self.troops = 500 #Set a constant in gamestate later?
        else: 
            self.settled = False
            self.troops = 0

    def fleetDefend(self,flt):
        self.defendingFleets.append(flt)
        self.fltDefend += flt.defend_r 

    def fleetRemove(self,flt):
        self.defendingFleets.remove(flt)
        self.fltDefend -= flt.defend_r         

    def attack(self,atkFlt):
       #attack defending fleet first, then planet's defenses
       #for now, just attack planet "health"
        self.health -= atkFlt.attack_r
        if (self.health < 0 ):
            GameVariables.win = True

    def fullstr(self):
        if (self.owner is not None):
            ownerstr = self.owner.name
        else:
            ownerstr = "no one"
        return self.name + " controlled by " + ownerstr + " at (" + str(self.x) + "," + str(self.y) + ")"

    def __str__(self):
        return self.name +  " (" + str(self.x) + "," + str(self.y) + ")"

