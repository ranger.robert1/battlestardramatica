#NameGenerator.py
#Generates names randomly picked
#from a names file, with a random integer
#suffix
import random
from random import SystemRandom

class NameGen:
    def __init__(self, rng):
        #uses an random number generator if provided, or generates one.
        if (rng is None):
            self.r = random.SystemRandom()
        else:
            self.r = rng
        self.initNames()

    def initNames(self):
        self.planets = list(open('res/planet_names.txt').read().split())
        self.fleets = list(open('res/fleet_names.txt').read().split())
        
    def GeneratePlanetName(self):
        name = str(self.r.choices(self.planets)[0])
        name += "_" + str(self.r.randint(0,9))
        return name

    def GenerateFleetName(self):
        name = str(self.r.choices(self.fleets)[0])
        name += "_" + str(self.r.randint(0,9))
        return name

