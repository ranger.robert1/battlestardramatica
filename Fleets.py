#GameState.py
#Manages all game interactions, moves, etc


class FltTypes:
    FLT_EXPL = 0
    FLT_ATTK = 1
    FLT_DFND = 2
    FLT_GTHR = 3
    def toString(fltType):
        if (fltType == FltTypes.FLT_EXPL):
            return "Explorer"
        elif (fltType == FltTypes.FLT_ATTK):
            return "Attacker"
        elif (fltType == FltTypes.FLT_DFND):
            return "Defender"
        elif (fltType == FltTypes.FLT_GTHR):
            return "Gather"

class FltActions:
    FLT_ACT_EXPL = 0
    FLT_ACT_ATTK = 1
    FLT_ACT_DFND = 2
    FLT_ACT_GTHR = 3

class FltGrp:
    fleet = []
    def __init__(self): #Generate an explore type fleet to start?
        #fleet.insert(0,Flt(FltTypes.FLT_EXPL,location))
        self.fleet = []
        self.cnt = 0
        print("TODO")

    def addFleet(self,flt):
        self.fleet.insert(0,flt)
        self.cnt += 1

    def removeFleet(self,flt):
        self.fleet.remove(flt)
        self.fleet -= 1

class Flt:
    attack_r = 0
    defend_r = 0
    gather_r = 0 
    health = 0
    speed = 0

    def __init__(self,fltType,location):
        self.name = GameVariables.nameGen.GeneratePlanetName()
        self.fltType = fltType
        self.location = location
        print(location.name)
        self.initStats()
        print("TODO")
    
    def defend(planet):
        print("TODO")

    def attack(planet):
        print("TODO")

    def initStats(self):
        if (self.fltType == FltTypes.FLT_EXPL):
            self.attack_r = 1
            self.defend_r = 1
            self.gather_r = 1
            self.speed = 5
            self.health = 10
        elif (self.fltType == FltTypes.FLT_ATTK):
            self.attack_r = 10
            self.defend_r = 5
            self.gather_r = 1
            self.speed = 2
            self.health = 10
        elif (self.fltType == FltTypes.FLT_DFND):
            self.attack_r = 5
            self.defend_r = 10
            self.gather_r = 1
            self.speed = 2
            self.health = 20

    def __str__(self):
        type_str=FltTypes.toString(self.fltType)
        loc_str = self.location.name
        misc_str = " (T:" + FltTypes.toString(self.fltType) + " H:" + str(self.health) + " A:" +  \
            str(self.attack_r) + " D:" + str(self.defend_r) +  " G:" + str(self.gather_r) + " S:" +  \
            str(self.speed) +")"

        return self.name + misc_str + " at " + str(self.location)

        

from GameState import GameVariables

from System import Planet
from System import Sys
